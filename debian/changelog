libmime-base32-perl (1.303-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libmime-base32-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 22 Oct 2022 10:28:03 +0100

libmime-base32-perl (1.303-2) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 15 Jun 2022 19:28:52 +0100

libmime-base32-perl (1.303-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Fri, 01 Jan 2021 15:20:59 +0100

libmime-base32-perl (1.303-1) unstable; urgency=medium

  * Team upload

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.
  * Remove Keith Lawson from Uploaders. Thanks for your work!

  [ Florian Schlichting ]
  * Import upstream version 1.303
  * Declare compliance with Debian Policy 4.1.0

 -- Florian Schlichting <fsfs@debian.org>  Sun, 17 Sep 2017 00:01:29 +0200

libmime-base32-perl (1.301-1) unstable; urgency=low

  * Team upload

  [ gregor herrmann ]
  * debian/copyright: set Maintainer and Copyright to their previous values.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Damyan Ivanov ]
  * Remove Martin F. Krafft from Uploaders on his request. Closes: #719087

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Florian Schlichting ]
  * Add debian/upstream/metadata
  * Import upstream version 1.301
  * Update upstream copyright, adding Chase Whitener and Jens Rehsack
  * Bump dh compat to level 9
  * Declare compliance with Debian Policy 3.9.8
  * Mark package autopkgtest-able
  * Delete obsolete dh_auto_install override

 -- Florian Schlichting <fsfs@debian.org>  Sun, 24 Apr 2016 22:47:40 +0200

libmime-base32-perl (1.02a-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/watch: relax regex for matching upstream versions.

  [ Keith Lawson ]
  * New upstream release
  * debian/control
    + add myself to uploaders
    + bump debhelper to >= 8.0.0
    + bump Standards-Version to 3.9.2
    + Build-Depends: debhelper (>= 8)
    + Build-Depends-Indep: perl
  * debian/copyright: changed upstream license stanza
  * Switch to dpkg-source 3.0 (quilt) format
  * Updated debian/copyright format (dh-make-perl refresh --only copyright)

 -- Keith Lawson <keith@nowhere.ca>  Sat, 23 Apr 2011 12:55:38 -0400

libmime-base32-perl (1.01-3) unstable; urgency=low

  * Upstream author has clarified licensing terms. Update debian/copyright.
    Closes: #545023

 -- Damyan Ivanov <dmn@debian.org>  Fri, 04 Sep 2009 17:49:46 +0300

libmime-base32-perl (1.01-2) unstable; urgency=low

  * Take over for the Debian Perl Group on maintainer's request
    (http://lists.alioth.debian.org/pipermail/pkg-perl-maintainers/2009-
    September/025053.html)
  * debian/control
    + add Vcs-* and Homepage fields
    + add ${misc:Depends} to Depends
    + Maintainer set to Debian Perl Group
  * debian/watch: use dist-based URL.
  * put myself in Uploaders
  * rules: convert to 3-line debhelper7; bump build-dependency; add compat
  * convert copyright to machine-readable format
  * bump Standards-Version to 3.8.3 (no changes needed)

 -- Damyan Ivanov <dmn@debian.org>  Fri, 04 Sep 2009 17:21:20 +0300

libmime-base32-perl (1.01-1) unstable; urgency=low

  * first Debian release of MIME::Base32
  * sponsored by Martin Krafft <madduck@debian.org>; thanks a lot.

 -- Adrian von Bidder <avbidder@fortytwo.ch>  Fri,  3 Dec 2004 13:55:55 +0100
